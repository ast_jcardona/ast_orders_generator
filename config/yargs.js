const createOrderOpts = {
    shippingOrderInfo : { //shippingOrderNumber:shippingStatus
        alias : 's',
        demand : true
    },
    productsInfo : { //productID:quatity:status,productID:quatity:status
        alias : 'p',
        demand : true
    },
    carrierInfo : { //carrier:carrierService
        alias : 'c',
        demand : false
    },
    locale : {
        alias: 'l',
        demand: true
    }
};

const createReturnOpts = {
    returnOrderInfo : { //OrderNumber:returnStatus:returnNumber
        alias : 'r',
        demand : true
    },
    productsInfo : { //productID:quatity:price,productID:quatity:price
        alias : 'p',
        demand : true
    },
    locale : {
        alias: 'l',
        demand: true
    }
}

const argv = require('yargs')
    .command('createOrder', 'Create an order passing arguments by console',  createOrderOpts )
    .command('createReturn', 'Create an order passing arguments by console',  createReturnOpts )
    .help()
    .argv

module.exports = {
    argv
};