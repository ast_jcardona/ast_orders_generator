const { writeFile} = require('./IOFile');
const { Builder } = require('xml2js');
const { setPropertyByPathOnXMLObject, getProductsInfo, getOrderInfo, getCarrierInfo, addProductsToXMLObject } = require('./helper')
const orderXMLObject = require('./orderXML.json');

let createOrder = (shippingOrderInfoConsole, productsInfoConsole, carrierInfoConsole, locale) => {
    return new Promise ((resolve, reject)=>{
        if (shippingOrderInfoConsole == true) {
            reject('shippingOrderInfo parameter musn\'t be empty');
            return;
        }
        if (productsInfoConsole == true) {
            reject('productsInfo parameter musn\'t be empty');
            return;
        }
        if (locale == true ) {
            reject('locale parameter musn\'t be empty');
            return;
        }

        var date = new Date().toISOString();
        var shippingOrderInfo = getOrderInfo(shippingOrderInfoConsole);
        var products = getProductsInfo(productsInfoConsole, shippingOrderInfo.orderNumber);
        var carrierInfo = getCarrierInfo(carrierInfoConsole);
        var trackingValue = '00000000' + Math.floor(Math.random() * (100 - 10) + 10);
        var XMLBuilder = new Builder();
        var xml;
        var propertiesToUpdate = {
            'shipping_order_status_feed.feed_description.created' : date,
            'shipping_order_status_feed.shipping_orders.shipping_order.shipping_order_number' : shippingOrderInfo.orderNumber,
            'shipping_order_status_feed.shipping_orders.shipping_order.ship_date' : date,
            'shipping_order_status_feed.shipping_orders.shipping_order.status' : shippingOrderInfo.orderStatus,
            'shipping_order_status_feed.shipping_orders.shipping_order.tracking_infos.tracking_info.carrier' : carrierInfo.carrier,
            'shipping_order_status_feed.shipping_orders.shipping_order.tracking_infos.tracking_info.carrier_service' : carrierInfo.carrierService,
            'shipping_order_status_feed.shipping_orders.shipping_order.tracking_infos.tracking_info.id' : trackingValue,
            'shipping_order_status_feed.shipping_orders.shipping_order.tracking_infos.tracking_info.tracking_number' : trackingValue + 'TN',
            'shipping_order_status_feed.shipping_orders.shipping_order.tracking_infos.tracking_info.ship_date' : date
        };
        
        for (propertie in propertiesToUpdate) {
            setPropertyByPathOnXMLObject(orderXMLObject, propertie, propertiesToUpdate[propertie])
        }
        
        addProductsToXMLObject(orderXMLObject, products, trackingValue, 'shipping_order_status_feed.shipping_orders.shipping_order.items.item');
        xml = XMLBuilder.buildObject(orderXMLObject);

        writeFile(`createdOrders/shipping_order_update_feed_${locale}_${date.replace(/\W|[A-Z]/g,'')}_f8c41d5d-1f62-1e36-e100-0000ac10040e.xml`, xml)
            .then( fileName => resolve(fileName))
            .catch( error => console.log(error));
    });
}

module.exports = {
    createOrder
}
