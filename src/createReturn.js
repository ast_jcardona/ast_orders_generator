const { writeFile} = require('./IOFile');
const { Builder } = require('xml2js');
const { setPropertyByPathOnXMLObject, getProductsInfo, getOrderInfo, addProductsToXMLObject } = require('./helper')
const orderXMLObject = require('./returnXML.json');

let createReturn = (returnOrderInfoConsole, productsInfoConsole, locale) => {
    return new Promise ((resolve, reject)=>{
        if (returnOrderInfoConsole == true) {
            reject('returnOrderInfo parameter musn\'t be empty');
            return;
        }
        if (productsInfoConsole == true) {
            reject('productsInfo parameter musn\'t be empty');
            return;
        }
        if (locale == true ) {
            reject('locale parameter musn\'t be empty');
            return;
        }

        var date = new Date().toISOString();
        var returnOrderInfo = getOrderInfo(returnOrderInfoConsole, true);
        var products = getProductsInfo(productsInfoConsole, returnOrderInfo.orderNumber, true);
        var XMLBuilder = new Builder();
        var xml;
        var propertiesToUpdate = {
            'return_import_feed.feed_description.created' : date,
            'return_import_feed.returns.return.order_no' : returnOrderInfo.orderNumber,
            'return_import_feed.returns.return.return_number' : returnOrderInfo.returnNumber,
            'return_import_feed.returns.return.status' : returnOrderInfo.orderStatus,
        };
        
        for (propertie in propertiesToUpdate) {
            setPropertyByPathOnXMLObject(orderXMLObject, propertie, propertiesToUpdate[propertie])
        }
        
        addProductsToXMLObject(orderXMLObject, products, null, 'return_import_feed.returns.return.items.item', true);
        xml = XMLBuilder.buildObject(orderXMLObject);

        writeFile(`createdReturns/return_import_feed_${locale}_${date.replace(/\W|[A-Z]/g,'')}_f04a155d-f8f0-8246-e100-0000ac10040f.xml`, xml)
            .then(fileName => resolve(fileName))
            .catch(error => console.log(error));
    });
}

module.exports = {
    createReturn
}
