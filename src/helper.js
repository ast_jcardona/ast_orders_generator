
function setPropertyByPathOnXMLObject(XMLObject, path, value) {
    let pathArray = path.split('.');
    var propertyToModify;
    pathArray.forEach((propertyName, index) => {
        if (index === 0) {
            propertyToModify = XMLObject[pathArray[0]];
        } else if (index == pathArray.length - 1) {
            propertyToModify[propertyName].push(value);
        } else {
            propertyToModify = propertyToModify[propertyName][0];
        }
    })
}

function getProductsInfo(productInfoString, OrderNumber, returnXML){
    var productsInfo = productInfoString.split(',');
    var products = [];
    productsInfo.forEach((productInfo, index) => {
        var productInfoSplited = productInfo.split(':');
        var productInfoProcessed = {
            itemID : OrderNumber + `#${index+1}`,
            productId : productInfoSplited[0],
            quatity : productInfoSplited[1] ? productInfoSplited[1] : '1',
        };

        if (returnXML) {
            if (!productInfoSplited[2]) {
                throw new Error('product price must be specified');
            }
            productInfoProcessed.price = productInfoSplited[2];
        } else {
            productInfoProcessed.status = productInfoSplited[2] ? productInfoSplited[2] : 'shipped'
        }

        products.push(productInfoProcessed);
    });

    return products;
}

function getOrderInfo(shippingOrderInfoString, returnXML){
    var orderInfoSplited = shippingOrderInfoString.split(':');
    var defaultStatus = returnXML ? 'completed' : 'shipped';
    var orderInfo = {
        orderNumber : orderInfoSplited[0] + '-1',
        orderStatus : orderInfoSplited[1] ? orderInfoSplited[1] : defaultStatus,
    };

    if (returnXML) {
        if (!orderInfoSplited[2]) {
            throw new Error('return number must be specified');
        }
        orderInfo.returnNumber = orderInfoSplited[2];
    }

    return orderInfo;
}

function getCarrierInfo(carrierInfoString){
    var carrierInfo = carrierInfoString ? carrierInfoString.split(':') : [];
    return {
        carrier : carrierInfo[0] ? carrierInfo[0] : 'ups',
        carrierService : carrierInfo[1] ? carrierInfo[1] : 'standar'
    }
}

function addProductsToXMLObject(XMLJSON, products, trackingValue, path, returnXML){
    products.forEach((product)=>{
        var productObj = {};
        if (returnXML) {
            productObj = {
                item_id: [
                    product.itemID
                ],
                product_id: [
                    product.productId
                ],
                quantity: [
                    product.quatity
                ],
                price: [
                    product.price
                ]
            };
        } else {
            productObj = {
                item_id: [
                    product.itemID
                ],
                product_id: [
                    product.productId
                ],
                quantity: [
                    product.quatity
                ],
                status: [
                    product.status
                ],
                tracking_refs: [
                    {
                        tracking_ref: [
                            {
                                quantity: [
                                    product.quatity
                                ],
                                ref: [
                                    trackingValue
                                ]
                            }
                        ]
                    }
                ]
            }
        }
        setPropertyByPathOnXMLObject(XMLJSON, path, productObj );
    });
}

module.exports = {
    setPropertyByPathOnXMLObject,
    getProductsInfo,
    getOrderInfo,
    getCarrierInfo,
    addProductsToXMLObject
}