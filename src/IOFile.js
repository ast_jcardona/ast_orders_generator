'use strict'
const fs = require('fs');


function readFile (path) {
    return new Promise ( (resolve, reject) => {
        fs.readFile(path, 'utf8', (error, data ) => {
            if ( error ){
                reject(error);
                return;
            }
            resolve(data);
        });
    } );
};

function writeFile (path, data) {
    return new Promise( (resolve, reject) => {
        fs.writeFile(path, data, error => {
            if (error) {
                reject(error);
                return;
            }

             resolve(path);
          });
    });
}

module.exports = {
    readFile,
    writeFile
}