const { createOrder } = require('./src/createOrder');
const { createReturn } = require('./src/createReturn');
const { argv } = require('./config/yargs');

let command = argv._[0];

switch (command) {
    case 'createOrder' : 
    createOrder(argv.shippingOrderInfo, argv.productsInfo, argv.carrierInfo, argv.locale )
        .then(fileName => console.log(`The file ${fileName} was successfully created`))
        .catch(error => console.log(error));
    break
    case 'createReturn' : 
    createReturn(argv.returnOrderInfo, argv.productsInfo, argv.locale )
        .then(fileName => console.log(`The file ${fileName} was successfully created`))
        .catch(error => console.log(error));
    break
    default:
        console.log('Command not found')
}
